package com.metis.placeholder.projectholder.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

//@CrossOrigin(origins = "http://localhost:8080")
@RestController
public class TestController {
	
	@RequestMapping(value = "/test", method = RequestMethod.GET, produces = "application/json")
	public String firstPage() {
		return "abc";
	}
}
