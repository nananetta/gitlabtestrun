import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HelloComponent } from './hello.component';
import { ShowcaseComponent } from '../showcase/showcase.component';
import { SearchbuttonComponent } from '../searchbutton/searchbutton.component';
import { LoginServiceService } from './login-service.service';

@NgModule({
  imports:      [ BrowserModule, FormsModule ],
  declarations: [ AppComponent, HelloComponent, ShowcaseComponent, SearchbuttonComponent],
  bootstrap:    [ AppComponent ],
  providers: [LoginServiceService]
})
export class AppModule { }
