import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-searchbutton',
  templateUrl: './searchbutton.component.html',
  styleUrls: ['./searchbutton.component.css']
})
export class SearchbuttonComponent implements OnInit {

  buttonLabel1:string = "button A";
  buttonLabel2:string = "button B";
  constructor() { }

  ngOnInit() {
  }

}